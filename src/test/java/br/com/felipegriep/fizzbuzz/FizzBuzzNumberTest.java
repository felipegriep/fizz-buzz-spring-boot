package br.com.felipegriep.fizzbuzz;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FizzBuzzNumberTest extends BaseTest {
    @Autowired
    private IFizzBuzzNumber fizzBuzzNumber;

    @Test
    public void fizzBuzzNumberNotNull() throws Exception {
        assertNotNull(fizzBuzzNumber);
    }

    @Test
    public void shouldReturnFizz() throws Exception {
        assertEquals(Constant.FIZZ, fizzBuzzNumber.proccess(3));
    }

    @Test
    public void shouldReturnBuzz() throws Exception {
        assertEquals(Constant.BUZZ, fizzBuzzNumber.proccess(5));
    }

    @Test
    public void shouldReturnFizzBuzz() throws Exception {
        assertEquals(Constant.FIZZ + Constant.BUZZ, fizzBuzzNumber.proccess(150));
    }

    @Test
    public void shouldReturn2() throws Exception {
        assertEquals("2", fizzBuzzNumber.proccess(2));
    }
}
