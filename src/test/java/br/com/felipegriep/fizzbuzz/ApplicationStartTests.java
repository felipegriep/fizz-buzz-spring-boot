package br.com.felipegriep.fizzbuzz;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)
@ActiveProfiles("test")
public class ApplicationStartTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void main() {
		ApplicationStart.main(new String[] {});
	}
}
