package br.com.felipegriep.fizzbuzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ApplicationStart {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(ApplicationStart.class, args);
		context.getBean(FizzBuzzNumber.class).proccessUntilHundred();
	}

}
