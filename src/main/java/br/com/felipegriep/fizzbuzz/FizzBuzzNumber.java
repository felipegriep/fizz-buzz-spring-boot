package br.com.felipegriep.fizzbuzz;

import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
public class FizzBuzzNumber implements IFizzBuzzNumber {

    public void proccessUntilHundred() {
        IntStream
                .rangeClosed(1, 100)
                .mapToObj(this::proccess)
                .forEach(System.out::println);
    }

    @Override
    public String proccess(int number) {
        String numberAsString = Integer.toString(number);
        StringBuilder response = new StringBuilder();
        if (number % 3 == 0 || numberAsString.contains("3")) {
            response.append(Constant.FIZZ);
        }
        if (number % 5 == 0 || numberAsString.contains("5")) {
            response.append(Constant.BUZZ);
        }

        if (response.toString().isEmpty()) {
            return numberAsString;
        }

        return response.toString();
    }
}
