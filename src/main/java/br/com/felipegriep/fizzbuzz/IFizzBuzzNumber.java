package br.com.felipegriep.fizzbuzz;

public interface IFizzBuzzNumber {
    void proccessUntilHundred();
    String proccess(int number);
}
