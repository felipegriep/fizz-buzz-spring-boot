package br.com.felipegriep.fizzbuzz;

public interface Constant {
    static final String FIZZ = "Fizz";
    static final String BUZZ = "Buzz";
}
